/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.util;

/**
 * Delta class is used to hold certain information in regards to the mouse 
 * movement and view (scale or translation)
 * 
 * @author Rustam Drake
 */
public class Delta {
    private double mousex;//the current mouse factor for X-axis
    private double mousey;
    private double translatex;
    private double translatey;
    private double scalex;//the current scale factor for X-axis
    private double scaley;//the current scale factor for Y-axis

    public double getTranslatex() {
        return translatex;
    }
    
    public void setTranslatex(double translatex) {
        this.translatex = translatex;
    }

    public double getTranslatey() {
        return translatey;
    }

    public void setTranslatey(double translatey) {
        this.translatey = translatey;
    }
    /**
     * get current scale factor for X-axis
     * @return scalex
     */
    public double getScalex() {
        return scalex;
    }
    /**
     * set scale factor for X-axis
     * @param scalex 
     */
    public void setScalex(double scalex) {
        this.scalex = scalex;
    }
    /**
     * get current scale factor for Y-axis
     * @return scaley
     */
    public double getScaley() {
        return scaley;
    }
    /**
     * set scale factor for Y-axis
     * @param scaley
     */
    public void setScaley(double scaley) {
        this.scaley = scaley;
    }
    /**
     * 
     * @return mouse clicked X-axis value
     */
    public double getMousex() {
        return mousex;
    }
    /**
     * set mouse clicked X-axis value
     * @param mousex 
     */
    public void setMousex(double mousex) {
        this.mousex = mousex;
    }

    public double getMousey() {
        return mousey;
    }

    public void setMousey(double mousey) {
        this.mousey = mousey;
    }
    
    
}
