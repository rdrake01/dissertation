/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.util;

import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import rd.project.model.EnrouteWaypoint;
import rd.project.model.Fix;
import rd.project.model.TerminalWaypoint;
import rd.project.model.wrapper.WaypointWrapper;

/**
 *
 * @author rustam.drake
 */
public class XMLUtil {

    public static <T extends Fix> void saveDataToFile(File file, List<T> data) {
        File tempFile = new File(file.getAbsolutePath()
                .substring(0, file.getAbsolutePath().length() - 4) + "_fix.xml");
        try {
            JAXBContext context = JAXBContext.newInstance(WaypointWrapper.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            WaypointWrapper wrapper = new WaypointWrapper();
                List<Fix> list =  (List<Fix>) data;
                wrapper.setFix(list);
                marshaller.marshal(wrapper, tempFile);

        } catch (JAXBException ex) {
//            Alert alert
            ex.printStackTrace();
        }
    }
    
}
