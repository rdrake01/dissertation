/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import rd.project.model.*;
import rd.project.model.Fix;
import rd.project.model.TerminalWaypoint;

/**
 *
 * @author Rustam Drake
 */
public class FileUtil {
    private static String file;
    /**
     * Read file and returns the content as a string
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readFile(File file) throws IOException {
        FileUtil.file = file.getPath();
        List<Fix> list = new ArrayList<>();
        StringBuilder sidStringBuilder = new StringBuilder();
        StringBuilder starStringBuilder = new StringBuilder();
        StringBuilder approachStringBuilder = new StringBuilder();
        BufferedReader reader = Files.newBufferedReader(file.toPath());
        
        try {
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.charAt(4) == 'P' && line.charAt(12) == 'C' && 
                        (line.charAt(21) == '1' || line.charAt(21) == '0')) {
                    list.add((Fix)TerminalWaypoint.stringToObject(line));
                }else if ("EA".equals(line.substring(4, 6)) && 
                        (line.charAt(21) == '1' || line.charAt(21) == '0')) {
                    list.add((Fix)EnrouteWaypoint.stringToObject(line));
                }else if ("D ".equals(line.substring(4, 6)) && 
                        (line.charAt(21) == '1' || line.charAt(21) == '0')) {
                    list.add((Fix)VHF.stringToObject(line));
                }else if (line.charAt(4)=='P' && line.charAt(12)=='A' &&
                        (line.charAt(21) == '1' || line.charAt(21) == '0')) {
                    list.add((Fix)Airport.stringToObject(line));
                }else if (line.charAt(4) == 'P' && line.charAt(12) == 'E'){
                    starStringBuilder.append(line).append("\n");
                }else if (line.charAt(4) == 'P' && line.charAt(12) == 'D'){
                    sidStringBuilder.append(line).append("\n");
                }else if (line.charAt(4) == 'P' && line.charAt(12) == 'F'){
                    approachStringBuilder.append(line).append("\n");
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            createFile(starStringBuilder.toString(), file, "star");
            createFile(sidStringBuilder.toString(), file, "sid");
            createFile(approachStringBuilder.toString(), file, "app");
            XMLUtil.saveDataToFile(file, list);
            reader.close();
        }
        reader.close();
        return "";
    }

    /**
     * Saves the content String to the specified file.
     *
     * @param content
     * @param file
     * @throws IOException thrown if an I/O error occurs opening or creating the
     * file
     */
//    public static void saveFile(String content, File file) throws IOException {
//        BufferedWriter writer = Files.newBufferedWriter(file.toPath());
//        writer.write(content, 0, content.length());
//        writer.close();
//    }

    private static void createFile(String content, File file, String type) throws IOException {
        File tempFile = new File(file.getAbsolutePath()
                .substring(0, file.getAbsolutePath().length() - 4) + "_" + type + ".txt");
        BufferedWriter writer = null;
        try {
            writer = Files.newBufferedWriter(tempFile.toPath());
            writer.write(content, 0, content.length());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    public static String getFile(){
        return FileUtil.file;
    }

}
