/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.test;

import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;

/**
 *
 * @author Rustam Drake
 */



//line1.setStrokeWidth(1/scale.getX());
public class LineTest extends Parent{
    private final Line line;
    public LineTest(){
        line = new Line(10, 10, 700, 900);
//        line.setStrokeWidth(20);
//        System.out.println("*** Stroke of line width: "+ line.getStrokeWidth());
        getChildren().add(line);
    }
    
    public LineTest(double strokeWidth){
        line = new Line(10, 10, 700, 900);
        line.setStrokeWidth(strokeWidth);
        System.out.println("*** Stroke of line width: "+ line.getStrokeWidth());
        getChildren().add(line);
    }
}
