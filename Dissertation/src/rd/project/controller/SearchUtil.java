/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import rd.project.model.Fix;
import rd.project.model.wrapper.WaypointWrapper;
import rd.project.util.FileUtil;
import rd.project.view.SearchView;

/**
 *
 * @author ali
 */
public class SearchUtil {

    private static List<String> procedureList;

    /**
     * @param fix_id
     * @return info a specified Fix id/name
     */
    public static Fix getFixObject(String fix_id) {
        WaypointWrapper wrapper = null;
        List<Fix> list = null;
        String str = null;
        try {
            str = FileUtil.getFile();
        } catch (Exception e) {
            if (str == null) {
                return null;
            }
        }

        File f = new File(str.substring(0, str.length() - 4) + "_fix.xml");
        try {
            JAXBContext context = JAXBContext.newInstance(WaypointWrapper.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            wrapper = (WaypointWrapper) unmarshaller.unmarshal(f);
        } catch (JAXBException ex) {
            Logger.getLogger(SearchView.class.getName()).log(Level.SEVERE, null, ex);
        }
        list = wrapper.getFix();
        if (list != null) {
            for (Fix waypoint : list) {
                if (waypoint.getFix_id().equals(fix_id)) {
                    return waypoint;
                }
            }
        }
        return null;
    }

    public static Fix getFixObject(String fix_id, String icao_code) {
        WaypointWrapper wrapper = null;
        List<Fix> list = null;
        String str = null;
        try {
            str = FileUtil.getFile();
        } catch (Exception e) {
            if (str == null) {
                return null;
            }
        }

        File f = new File(str.substring(0, str.length() - 4) + "_fix.xml");
        try {
            JAXBContext context = JAXBContext.newInstance(WaypointWrapper.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            wrapper = (WaypointWrapper) unmarshaller.unmarshal(f);
        } catch (JAXBException ex) {
            Logger.getLogger(SearchView.class.getName()).log(Level.SEVERE, null, ex);
        }
        list = wrapper.getFix();
        if (list != null) {
            for (Fix fix : list) {
                if (fix.getFix_id().equals(fix_id) && fix.getIcao_code().equals(icao_code)) {
                    return fix;
                }
            }
        }
        return null;
    }

    public static List<Fix> getFixObjects(String fix_id) {
        WaypointWrapper wrapper = null;
        List<Fix> list = null;
        List<Fix> result = new ArrayList<>();
        String str = null;
        try {
            str = FileUtil.getFile();
        } catch (Exception e) {
            if (str == null) {
                return null;
            }
        }

        File f = new File(str.substring(0, str.length() - 4) + "_fix.xml");
        try {
            JAXBContext context = JAXBContext.newInstance(WaypointWrapper.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            wrapper = (WaypointWrapper) unmarshaller.unmarshal(f);
        } catch (JAXBException ex) {
            Logger.getLogger(SearchView.class.getName()).log(Level.SEVERE, null, ex);
        }
        list = wrapper.getFix();
        if (list != null) {
            for (Fix waypoint : list) {
                if (waypoint.getFix_id().equals(fix_id)) {
                    result.add(waypoint);
                }
            }
        }
        return result;
    }

    public static String getProceduresByICAO(String fourLetterAirportCode) {
        String procedures = "---[STAR]--- \n";
        String str = null;
        try {
            str = FileUtil.getFile();
        } catch (Exception e) {
            if (str == null) {
                return "no File selected";
            }
        }
        File sid = new File(str.substring(0, str.length() - 4) + "_sid.txt");
        File star = new File(str.substring(0, str.length() - 4) + "_star.txt");
        File approach = new File(str.substring(0, str.length() - 4) + "_app.txt");

        BufferedReader readerStar = null;
        BufferedReader readerSid = null;
        BufferedReader readerApp = null;
        try {
            int i = 0;
            readerStar = Files.newBufferedReader(star.toPath());
            readerSid = Files.newBufferedReader(sid.toPath());
            readerApp = Files.newBufferedReader(approach.toPath());
            String line = null;
            while ((line = readerStar.readLine()) != null) {
                if (line.substring(6, 10).equals(fourLetterAirportCode)) {
                    procedures = procedures + line + "\n";
                }
            }
            while ((line = readerSid.readLine()) != null) {
                if (line.substring(6, 10).equals(fourLetterAirportCode)) {
                    procedures = procedures + line + "\n";
                }
            }
            while ((line = readerApp.readLine()) != null) {
                if (line.substring(6, 10).equals(fourLetterAirportCode)) {
                    procedures = procedures + line + "\n";
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            
            if (readerStar != null||readerSid!=null||readerApp!=null) {
                try {
                    readerStar.close();
                    readerSid.close();
                    readerApp.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
//        readerStar.close();
        return procedures;
    }

    /**
     * the methods searches for a unique procedure
     *
     * @param ICAO airport 4 letter code
     * @param ident procedure ident
     * @param type procedure type [D = sid, E = star, F = Approach]
     * @return a single unique procedure
     */
    public static List<String> getProceduresByICAOAndProcedureIdent(String ICAO, String ident, char type) {
        procedureList = new ArrayList<>();
        File file = null;
//        String procedure = "";
        String str = null;
        try {
            str = FileUtil.getFile();
        } catch (Exception e) {
            if (str == null) {
                System.out.println("null");
                return null;
            }
        }
        switch (type) {
            case 'E':
                file = new File(str.substring(0, str.length() - 4) + "_star.txt");
                break;
            case 'D':
                file = new File(str.substring(0, str.length() - 4) + "_sid.txt");
                break;
            case 'F':
                file = new File(str.substring(0, str.length() - 4) + "_app.txt");
                break;
        }
        BufferedReader reader = null;
        try {
            int i = 0;
            reader = Files.newBufferedReader(file.toPath());
            String line = null;
            while ((line = reader.readLine()) != null) {
//                System.out.println("line: " + line);
                if (line.substring(13, 19).trim().equals(ident)) {
                    if (line.substring(6, 10).equals(ICAO) && line.charAt(38) == '1') {
                        procedureList.add(line);
                    }

                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return procedureList;
    }

    public static List<String> getProcedureList() {
        return procedureList;
    }

    public static void setProcedureList(List<String> procedureList) {
        SearchUtil.procedureList = procedureList;
    }

}
