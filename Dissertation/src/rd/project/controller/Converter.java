/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.controller;

import rd.project.model.Fix;

/**
 *
 * @author Rustam Drake
 */
public class Converter {
    
    private double x;
    private double y;
    private final static double RADIUS =6371;
    public Converter() {
    }
    /**
     * 
     * @param hourMinSec
     * @return converted version of Hours Min Sec as a Decimal, for calculation purposes
     * ie. W031431201 ("W031 43 12.01") => -31.720002777777776
     */
    public double hourMinSec_to_Decimal(String hourMinSec) throws NumberFormatException{
        double result = 0;
        if(hourMinSec.length()==9){
            double hour = Double.parseDouble( hourMinSec.substring(1, 3));
            double min = Double.parseDouble( hourMinSec.substring(3, 5));
            double sec = Double.parseDouble( hourMinSec.substring(5,7) + "."+hourMinSec.substring(7));
            result = hour+min/60d+sec/3600d;
//            System.out.println("h: "+hour + " |min: " + min + " |sec: "+sec);

        }else if(hourMinSec.length()==10){
            double hour = Double.parseDouble( hourMinSec.substring(1, 4));
            double min = Double.parseDouble( hourMinSec.substring(4, 6));
            double sec = Double.parseDouble( hourMinSec.substring(6, 8) + "."+hourMinSec.substring(8));
            result = hour+min/60d+sec/3600d;
//            System.out.println("h: "+hour + " |min: " + min + " |sec: "+sec);

        }
        if(result == 0) return Double.NaN;
        else if(hourMinSec.charAt(0)=='N' || hourMinSec.charAt(0)== 'E'){
            return result * Math.PI / 180;
        }else return ((-1d)*result)* Math.PI / 180;
        
    }
    
    public double convertToCartesianX(Fix fix){
        this.x = RADIUS * Math.sin(hourMinSec_to_Decimal(fix.getLatitude()))
                * Math.cos(hourMinSec_to_Decimal(fix.getLongitude()));
        return this.x;
    }
    
    public double convertToCartesianY(Fix fix){
        this.y = RADIUS * Math.sin(hourMinSec_to_Decimal(fix.getLatitude())* Math.PI / 180)
                * Math.sin(hourMinSec_to_Decimal(fix.getLongitude())* Math.PI / 180);
        return this.y;
    }
    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
    /**
     * 
     * @return mean radius of earth in KM 
     */
    public static double getRADIUS() {
        return RADIUS;
    }
    
}
