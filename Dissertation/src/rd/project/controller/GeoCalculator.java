/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import rd.project.model.Fix;

/**
 *
 * @author rustam.drake
 */
public class GeoCalculator {
    /**
     * using the following formula to obtain a distance between given point:<br>
     * a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)<br>
     * c = 2 ⋅ atan2(√a, √(1−a))<br>
     * d = R ⋅ c<br>
     * where φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
     * note that angles need to be in radians to pass to trig functions!<br>
     * formula obtained from http://www.movable-type.co.uk/scripts/latlong.html
     * @param fix1
     * @param fix2
     * @return distance in KM
     */
    public static double distanceBetweenTwoPoints(Fix fix1, Fix fix2) {
        Converter converter = new Converter();
        double R = Converter.getRADIUS();
        double latRadians1 = converter.hourMinSec_to_Decimal(fix1.getLatitude()) ;
        double latRadians2 = converter.hourMinSec_to_Decimal(fix2.getLatitude()) ;
        double deltaLat = (converter.hourMinSec_to_Decimal(fix1.getLatitude())
                - converter.hourMinSec_to_Decimal(fix2.getLatitude())) ;
        double deltaLon = (converter.hourMinSec_to_Decimal(fix1.getLongitude())
                - converter.hourMinSec_to_Decimal(fix2.getLongitude())) ;
        
        double a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2)
                + Math.cos(latRadians1) * Math.cos(latRadians2)
                * Math.sin(deltaLon/2) * Math.sin(deltaLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c * 0.539956803;
        DecimalFormat df = new DecimalFormat("#.#####");
        return Double.parseDouble(df.format(d));
    }
    
    public double distanceBetweenTwoPoints(String lat1, String lon1, String lat2, String lon2) {
        Converter converter = new Converter();
        double R = Converter.getRADIUS();
        double latRadians1 = converter.hourMinSec_to_Decimal(lat1) ;
        double latRadians2 = converter.hourMinSec_to_Decimal(lat2) ;
        
        double deltaLat = (converter.hourMinSec_to_Decimal(lat2)
                - converter.hourMinSec_to_Decimal(lat1)) ;
        double deltaLon = (converter.hourMinSec_to_Decimal(lon2)
                - converter.hourMinSec_to_Decimal(lon1)) ;
        
        double a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2)
                + Math.cos(latRadians1) * Math.cos(latRadians2)
                * Math.sin(deltaLon/2) * Math.sin(deltaLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c * 0.539956803;
        DecimalFormat df = new DecimalFormat("#.#####");
        return Double.parseDouble(df.format(d));
    }
}
