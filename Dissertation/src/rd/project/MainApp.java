/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project;

import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Scale;
import static javafx.scene.transform.Transform.scale;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import rd.project.util.Delta;
import rd.project.view.GeoCalculatorView;
import rd.project.view.PlotView;
import rd.project.view.SearchView;

/**
 * test
 *
 * @author Rustam Drake
 */
public class MainApp extends Application {

    private Stage stage;
    private BorderPane root;
    private TabPane tabPane;
    private RootLayoutController rootController;
    private Scene scene;
    private SearchView searchView;
    private double stageHeight;
    private double stageWidth;
    private PlotView plotView;
//    private Scale scale;

    @Override
    public void start(Stage stage) {
        this.stage = stage;
        this.stage.setTitle("Dissertation");
        this.stage.setMinWidth(620);
        this.stage.setMinHeight(200);
        initRoot();
        showTabPane();
    }

    public void initRoot() {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("RootLayout.fxml"));
            root = (BorderPane) loader.load();
            scene = new Scene(root);

//            scene.widthProperty().addListener(new ChangeListener<Number>() {
//                @Override 
//                public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
//                    searchView.setWidthOfWindow((double)newSceneWidth);
//                    System.out.println("Width: " + newSceneWidth);
//                }
//
//            });
            this.rootController = loader.getController();
            rootController.setMainApp(this);

            stage.setScene(scene);
            stage.show();

//            this.stageHeight = stage.getHeight();
//            this.stageWidth = stage.getWidth();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showTabPane() {
        searchView = new SearchView(stage);
        tabPane = new TabPane();
        Tab tabDraw = new Tab("Draw");
        tabDraw.setClosable(false);
        tabDraw.setContent(plot());

        Tab tabConfig = new Tab("Config");
        Tab tabBearingAndDistance = new Tab("Brg & Dist");
        tabBearingAndDistance.setClosable(false);
        tabBearingAndDistance.setContent(new GeoCalculatorView());

        Tab tabAriOutput = new Tab("ARI");
        tabAriOutput.setClosable(false);
        tabAriOutput.setContent(ariOutput());

        Tab searchTab = new Tab("Search");
        searchTab.setContent(searchView);

        tabPane.getTabs().add(tabBearingAndDistance);
        tabPane.getTabs().add(tabDraw);
        tabPane.getTabs().add(searchTab);
        tabPane.getTabs().add(tabAriOutput);
        tabPane.getTabs().add(tabConfig);

        root.setCenter(tabPane);

    }

    public Parent ariOutput() {
        Pane pane = new Pane();
        File file = rootController.getFile();
        Text textAri = null;
        if (file != null) {
            textAri = new Text("file with following path selected: " + file.getAbsolutePath());
        } else {
            textAri = new Text("no file has been selected");
        }
        textAri.setTextOrigin(VPos.TOP);
        textAri.setTextAlignment(TextAlignment.JUSTIFY);
        textAri.setWrappingWidth(scene.getWidth() - 50);//doest work, should wrap -
//        -the text so all contetnt is seen

        Group textGroup = new Group(textAri);
        textGroup.setLayoutX(50);
        textGroup.setLayoutY(180);
        textGroup.setClip(new Rectangle(480, 85));
        pane.getChildren().add(textGroup);

        return pane;
    }

    public BorderPane plot() {
        final Delta delta = new Delta();
        final double SCALE_DELTA = 1.1;
        delta.setScalex(1);

        BorderPane borderPane = new BorderPane();
        borderPane.setId("pane");
        borderPane.setStyle("-fx-background-color: chocolate");

//        String tabStyleSheetPath = this.getClass().getResource("css/drawTabStyle.css").toExternalForm();
//        sp.getStylesheets().add(tabStyleSheetPath);
//        sp.setStyle("-fx-background-color: chocolate");
        this.plotView = new PlotView();

        borderPane.setOnScroll((ScrollEvent event) -> {
            event.consume();

            if (event.getDeltaY() == 0) {
                return;
            }
            double currentScale = 1;
            double scaleFactor = (event.getDeltaY() > 0) ? SCALE_DELTA : 1 / SCALE_DELTA;
//            delta.setScalex(plotView.getScaleX() * scaleFactor);
//            delta.setScaley(plotView.getScaleY() * scaleFactor);
//            plotView.setScale(delta.getScalex());
//            plotView.update();
//            plotView.setScaleX(delta.getScalex());
//            plotView.setScaleY(delta.getScaley());

            delta.setScalex(scaleFactor * delta.getScalex());
            Scale scale = new Scale();
            scale.setPivotX(event.getX());
            scale.setPivotY(event.getY());
            System.out.println("pivot X: " + scale.getPivotX());
            System.out.println("pivot Y: " + scale.getPivotY());
            scale.setX(scaleFactor);
            scale.setY(scaleFactor);
            currentScale = scaleFactor * currentScale;
            plotView.setScale(delta.getScalex());
            plotView.update();
            System.out.println("scale: " + scale.getX());
//            VCBI ANUT2A
            plotView.getTransforms().add(scale);
        });

        borderPane.setOnMousePressed((MouseEvent event) -> {
            System.out.println("Scene - h: " + scene.getHeight());
            delta.setMousex(event.getSceneX());
            delta.setMousey(event.getSceneY() - 60);
            delta.setTranslatex(plotView.getTranslateX());
            delta.setTranslatey(plotView.getTranslateY());
        });

        borderPane.setOnMouseDragged((MouseEvent event) -> {
            plotView.setTranslateX(delta.getTranslatex() + (event.getX() - delta.getMousex()));
            plotView.setTranslateY(delta.getTranslatey() + (event.getY() - delta.getMousey()));

//            Translate translate = new Translate();
//            translate.setX((event.getX() - delta.getMousex()) / 10);
//            translate.setY((event.getY() - delta.getMousey()) / 10);
//            System.out.println("event.getX(): " + event.getX());
//            System.out.println("delta.getMousex: " + delta.getMousex());
//            System.out.println("translate X: " + translate.getX());
//            System.out.println("translate Y: " + translate.getY());
//            plotView.getTransforms().add(translate);

        });
        VBox vBox = new VBox();
//        vBox.setPadding(new Insets(5));
        vBox.setSpacing(2);
        vBox.setStyle("-fx-background-color: black");
        vBox.setPrefWidth(100);
        vBox.setMinWidth(100);

//        sp.getChildren().add(plotView);
        TextField icaoTextField = new TextField();
        icaoTextField.setMinWidth(50);

        Button resetButton = new Button("Reset");
        resetButton.setPrefWidth(70);
        resetButton.setOnAction((ActionEvent event) -> {
            plotView.setTranslateX(0);
            plotView.setTranslateY(0);
            Scale scale = new Scale();
            scale.setX(1);
            scale.setY(1);
//            currentScale = scaleFactor * currentScale;
            plotView.setScale(1);
            plotView.update();
            System.out.println("scale: " + scale.getX());
//            VCBI ANUT2A
            plotView.getTransforms().add(scale);
        });

        vBox.getChildren().addAll(resetButton, icaoTextField);

        borderPane.setCenter(plotView);
        borderPane.setRight(vBox);
//        sp.getChildren().addAll(plotView, vBox);

        icaoTextField.setOnKeyPressed((KeyEvent key) -> {
            if (key.getCode() == KeyCode.ENTER) {
                String str[] = icaoTextField.getText().split(" ");

                borderPane.getChildren().clear();
                this.plotView = new PlotView(str[0].toUpperCase(), str[1].toUpperCase().trim());
                borderPane.getChildren().add(plotView);
                borderPane.setRight(vBox);
            }
        });
        return borderPane;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    //***getters and setters***//
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public double getStageHight() {
        return stageHeight;
    }

    public double getStageWidth() {
        return stageWidth;
    }

}
