/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model.procedure;

import java.util.ArrayList;
import java.util.List;
import rd.project.controller.SearchUtil;
import rd.project.model.procedure.path_terminators.*;

/**
 *
 * @author Rustam Drake
 */
public class Star {
    private String proc_id;
    private String aeroport_code;
//    private String procedure;
    private String transition_level;
    private int cycle;
    private String runway;
    private String area;
    private List<String> procedureList;
    
    public Star (List<String> procedureList){
        this.procedureList = procedureList;
        String first_leg = procedureList.get(0);
        this.area = first_leg.substring(1, 4);
        this.proc_id = first_leg.substring(13, 19);
        this.aeroport_code = first_leg.substring(6, 10);
        this.transition_level = first_leg.substring(94, 99);
        this.cycle = Integer.parseInt(first_leg.substring(128, 132));
        this.runway = first_leg.substring(20, 25);
    }
    
    
    public  List<Leg> getProcedureAsList(){
      List<Leg> legSeqList = new ArrayList<>();
      List<String> list = this.procedureList;
      for(String s : list){
          switch (s.substring(47, 49))
          {
              case "IF":
                  legSeqList.add(new IFLeg(s));
                  break;
              case "TF":
                  legSeqList.add(new TFLeg(s));
                  break;
          }
      }
      
      return legSeqList;
    } 

    public String getProc_id() {
        return proc_id;
    }

    public void setProc_id(String proc_id) {
        this.proc_id = proc_id;
    }

    public String getAeroport_code() {
        return aeroport_code;
    }

    public void setAeroport_code(String aeroport_code) {
        this.aeroport_code = aeroport_code;
    }

    public String getTransition_level() {
        return transition_level;
    }

    public void setTransition_level(String transition_level) {
        this.transition_level = transition_level;
    }

    public int getCycle() {
        return cycle;
    }

    public void setCycle(int cycle) {
        this.cycle = cycle;
    }

    public String getRunway() {
        return runway;
    }

    public void setRunway(String runway) {
        this.runway = runway;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<String> getProcedureList() {
        return procedureList;
    }

    public void setProcedureList(List<String> procedureList) {
        this.procedureList = procedureList;
    }
    
    
}
