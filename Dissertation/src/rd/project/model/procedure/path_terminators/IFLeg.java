/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model.procedure.path_terminators;

import java.util.Objects;

/**
 *
 * @author Rustam Drake
 */
public class IFLeg implements Leg{
    private String seq;
    private String fix_id;
    private String fix_icao_code;
    private String fix_type;
    private String altitude;
    private String speed;

    public IFLeg(String leg){
//        super(leg);
        this.seq = leg.substring(26, 29);
        this.fix_id = leg.substring(29, 34);
        this.fix_icao_code = leg.substring(34, 36);
        this.altitude = leg.substring(82, 83) + leg.substring(84, 89);
        this.speed = leg.substring(117, 118) + leg.substring(99, 102);
    }

    @Override
    public String getSeq() {
        return seq;
    }

    @Override
    public void setSeq(String seq) {
        this.seq = seq;
    }

    @Override
    public String getFix_id() {
        return fix_id;
    }

    @Override
    public void setFix_id(String fix_id) {
        this.fix_id = fix_id;
    }

    @Override
    public String getFix_icao_code() {
        return fix_icao_code;
    }

    @Override
    public void setFix_icao_code(String fix_icao_code) {
        this.fix_icao_code = fix_icao_code;
    }

    public String getFix_type() {
        return fix_type;
    }

    public void setFix_type(String fix_type) {
        this.fix_type = fix_type;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.seq);
        hash = 89 * hash + Objects.hashCode(this.fix_id);
        hash = 89 * hash + Objects.hashCode(this.fix_type);
        hash = 89 * hash + Objects.hashCode(this.altitude);
        hash = 89 * hash + Objects.hashCode(this.speed);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IFLeg other = (IFLeg) obj;
        if (this.speed != other.speed) {
            return false;
        }
        if (!Objects.equals(this.seq, other.seq)) {
            return false;
        }
        if (!Objects.equals(this.fix_id, other.fix_id)) {
            return false;
        }
        if (!Objects.equals(this.fix_type, other.fix_type)) {
            return false;
        }
        if (!Objects.equals(this.altitude, other.altitude)) {
            return false;
        }
        return true;
    }
    
    
}
