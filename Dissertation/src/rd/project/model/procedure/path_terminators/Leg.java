/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model.procedure.path_terminators;

/**
 *
 * @author Rustam Drake
 */
public interface Leg {
//    String seq ="";
//    String fix_id ="";
//    String fix_icao_code = "";
//    String fix_type ="";
//    String altitude ="";
//    String speed ="";
    
    public String getSeq();

    public void setSeq(String seq);

    public String getFix_id();

    public void setFix_id(String fix_id);
    
    public String getFix_icao_code();

    public void setFix_icao_code(String fix_icao_code);
//
//    public String getFix_type() {
//        return fix_type;
//    }
//
//    public void setFix_type(String fix_type) {
//        this.fix_type = fix_type;
//    }
//
//    public String getAltitude() {
//        return altitude;
//    }
//
//    public void setAltitude(String altitude) {
//        this.altitude = altitude;
//    }
//
//    public String getSpeed() {
//        return speed;
//    }
//
//    public void setSpeed(String speed) {
//        this.speed = speed;
//    }
}
