/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rustam Drake
 */
public class TerminalWaypoint extends Fix{
//    private String aeroport_code;
//
//    public String getAeroport_code() {
//        return aeroport_code;
//    }
//
//    public void setAeroport_code(String aeroport_code) {
//        this.aeroport_code = aeroport_code;
//    }

    public static TerminalWaypoint stringToObject(String string){
        int a = 0, b = 1, c = 4, d = 6, e = 10, f = 12, g = 13, h = 18, k = 32, l = 41, m = 51;

            TerminalWaypoint waypoint = new TerminalWaypoint();
            waypoint.setArea(string.substring(b, c));
            waypoint.setAeroport_code(string.substring(d, e));
            waypoint.setIcao_code(string.substring(e, f));
            waypoint.setFix_id(string.substring(g, h));
            waypoint.setLatitude(string.substring(k, l));
            waypoint.setLongitude(string.substring(l, m));

        return waypoint;
    }
    
    public  List<TerminalWaypoint> stringToObject(StringBuffer string){
        List<TerminalWaypoint> list = new ArrayList<>();
        int a = 0, b = 1, c = 4, d = 6, e = 10, f = 12, g = 13, h = 18, k = 32, l = 41, m = 51;

        while (true) {
            TerminalWaypoint waypoint = new TerminalWaypoint();
            waypoint.setArea(string.substring(b, c));
            waypoint.setAeroport_code(string.substring(d, e));
            waypoint.setIcao_code(string.substring(e, f));
            waypoint.setFix_id(string.substring(g, h));
            waypoint.setLatitude(string.substring(k, l));
            waypoint.setLongitude(string.substring(l, m));

            if (list.isEmpty()) {
                list.add(waypoint);
            } else if (!list.get(list.size()-1).equals(waypoint)) {
                list.add(waypoint);

            }
            int value = 133;

            a += value;
            b += value;
            c += value;
            d += value;
            e += value;
            f += value;
            g += value;
            h += value;
            k += value;
            l += value;
            m += value;
            if (m > string.length()) {
                break;
            }
        }
        return list;
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }
    
    
//    @Override
//    public int hashCode() {
//        int hash = 7;
//        hash = 89 * hash + Objects.hashCode(this.aeroport_code);
//        hash = super.hashCode();
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final TerminalWaypoint other = (TerminalWaypoint) obj;
//        if (!Objects.equals(this.aeroport_code, other.aeroport_code)) {
//            return false;
//        }
//        return super.equals(obj);
//    }
//
//    @Override
//    public String toString() {
//        return "{aeroport_code=" + aeroport_code +", "+super.toString();
//    }

    
}