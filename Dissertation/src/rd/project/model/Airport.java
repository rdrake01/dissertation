/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model;

/**
 *
 * @author rustam.drake
 */
public class Airport extends Fix{
    
    
    public static Airport stringToObject(String string){
        int a = 0, b = 1, c = 4, d = 6, e = 10, f = 12, g = 13, h = 18, k = 32, l = 41, m = 51;
            Airport airport = new Airport();
            airport.setArea(string.substring(b, c));
            airport.setAeroport_code(string.substring(6, 10));
            airport.setIcao_code(string.substring(10, 12));
            airport.setFix_id(string.substring(6, 10));
            airport.setLatitude(string.substring(32, 41));
            airport.setLongitude(string.substring(41, 51));
            airport.setMag_var(string.substring(51, 56));
        return airport;
    }
}
