package rd.project.model;

import java.util.Objects;


public class VHF extends Fix {
    
    public static VHF stringToObject(String string){
        int a = 0, b = 1, c = 4, d = 6, e = 10, f = 12, g = 13, h = 18, k = 32, l = 41, m = 51;

            VHF vhf = new VHF();
            vhf.setArea(string.substring(b, c));
            vhf.setAeroport_code(string.substring(d, e));
            vhf.setIcao_code(string.substring(e, f));
            vhf.setFix_id(string.substring(13, 16).trim());
            vhf.setLatitude(string.substring(k, l));
            vhf.setLongitude(string.substring(l, m));
            vhf.setDME_lat_lon(string.substring(55, 74));
            vhf.setVhf_name(string.substring(93, 123).trim());
            vhf.setMag_var(string.substring(74, 79));
            
        return vhf;
    }

}