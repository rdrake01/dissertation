/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author Rustam Drake
 */
public class Fix {
    private String type;
    private String s_t;
    private String area;
    private String icao_code;
    private String fix_id;
    private String latitude;
    private String longitude;
    private String aeroport_code;
    private String vhf_name;
    private String stat_dec;
    private String mag_var;
    private String DME_lat_lon;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getAeroport_code() {
        return aeroport_code;
    }

    public void setAeroport_code(String aeroport_code) {
        this.aeroport_code = aeroport_code;
    }
    
    public String getS_t() {
        return s_t;
    }

    public void setS_t(String s_t) {
        this.s_t = s_t;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIcao_code() {
        return icao_code;
    }

    public void setIcao_code(String icao_code) {
        this.icao_code = icao_code;
    }

    public String getFix_id() {
        return fix_id;
    }

    public void setFix_id(String fix_id) {
        this.fix_id = fix_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getVhf_name() {
        return vhf_name;
    }

    public void setVhf_name(String vhf_name) {
        this.vhf_name = vhf_name;
    }

    public String getStat_dec() {
        return stat_dec;
    }

    public void setStat_dec(String stat_dec) {
        this.stat_dec = stat_dec;
    }

    public String getMag_var() {
        return mag_var;
    }

    public void setMag_var(String mag_var) {
        this.mag_var = mag_var;
    }

    public String getDME_lat_lon() {
        return DME_lat_lon;
    }

    public void setDME_lat_lon(String DME_lat_lon) {
        this.DME_lat_lon = DME_lat_lon;
    }
    
    
    
    public  <T extends Fix> List<T> stringToObject(StringBuffer String){
        return null;
    }
    
//    public  <T extends Fix> T stringToObject(StringBuffer String){
//        return null;
//    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.type);
        hash = 53 * hash + Objects.hashCode(this.s_t);
        hash = 53 * hash + Objects.hashCode(this.area);
        hash = 53 * hash + Objects.hashCode(this.icao_code);
        hash = 53 * hash + Objects.hashCode(this.fix_id);
        hash = 53 * hash + Objects.hashCode(this.latitude);
        hash = 53 * hash + Objects.hashCode(this.longitude);
        hash = 53 * hash + Objects.hashCode(this.aeroport_code);
        hash = 53 * hash + Objects.hashCode(this.vhf_name);
        hash = 53 * hash + Objects.hashCode(this.stat_dec);
        hash = 53 * hash + Objects.hashCode(this.mag_var);
        hash = 53 * hash + Objects.hashCode(this.DME_lat_lon);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fix other = (Fix) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.s_t, other.s_t)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.icao_code, other.icao_code)) {
            return false;
        }
        if (!Objects.equals(this.fix_id, other.fix_id)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.aeroport_code, other.aeroport_code)) {
            return false;
        }
        if (!Objects.equals(this.vhf_name, other.vhf_name)) {
            return false;
        }
        if (!Objects.equals(this.stat_dec, other.stat_dec)) {
            return false;
        }
        if (!Objects.equals(this.mag_var, other.mag_var)) {
            return false;
        }
        if (!Objects.equals(this.DME_lat_lon, other.DME_lat_lon)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Fix{" + "type=" + type + ", s_t=" + s_t + ", area=" + area 
                + ", icao_code=" + icao_code + ", fix_id=" + fix_id + ", latitude=" 
                + latitude + ", longitude=" + longitude + ", aeroport_code=" 
                + aeroport_code + ", vhf_name=" + vhf_name + ", stat_dec=" 
                + stat_dec + ", mag_var=" + mag_var + ", DME_lat_lon=" + DME_lat_lon + '}';
    }

    
    
}
