/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model.wrapper;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import rd.project.model.EnrouteWaypoint;
import rd.project.model.Fix;
import rd.project.model.TerminalWaypoint;

/**
 *
 * @author rustam.drake
 */

@XmlRootElement(name = "waypoints")
public class WaypointWrapper {
//    private List<TerminalWaypoint> terminalWaypoints;
//    private List<EnrouteWaypoint> enrouteWaypoints;
    private List<Fix> fix;
    
    @XmlElement(name = "fix")
    public List<Fix> getFix() {
        return fix;
    }

    public void setFix(List<Fix> fix) {
        this.fix = fix;
    }
    
    
    
    
//    @XmlElement(name = "terminal_waypoint")
//    public List<TerminalWaypoint> getTerminalWaypoints(){
//        return terminalWaypoints;
//    }
//
//    public void setTerminalWaypoints(List<TerminalWaypoint> terminalWaypoints) {
//        this.terminalWaypoints = terminalWaypoints;
//    }
//    
//    @XmlElement(name = "enroute_waypoint")
//    public List<EnrouteWaypoint> getEnrouteWaypoints() {
//        return enrouteWaypoints;
//    }
//
//    public void setEnrouteWaypoints(List<EnrouteWaypoint> enrouteWaypoints) {
//        this.enrouteWaypoints = enrouteWaypoints;
//    }
//    
//    
    
}
