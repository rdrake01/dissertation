/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rustam Drake
 */
public class EnrouteWaypoint extends Fix{

    @Override
    public List<EnrouteWaypoint> stringToObject(StringBuffer string) {
        
        List<EnrouteWaypoint> list = new ArrayList<>();
        int a = 0, b = 1, c = 4, d = 6, e = 10, f = 12, g = 13, h = 18, k = 32, l = 41, m = 51;

        while (true) {
            EnrouteWaypoint waypoint = new EnrouteWaypoint();
            waypoint.setArea(string.substring(b, c));
//            waypoint.setAeroport_code(string.substring(d, e));
            waypoint.setIcao_code(string.substring(19, 21));
            waypoint.setFix_id(string.substring(g, h));
            waypoint.setLatitude(string.substring(k, l));
            waypoint.setLongitude(string.substring(l, m));

            if (list.isEmpty()) {
                list.add(waypoint);
            } else if (!list.get(list.size()-1).equals(waypoint)) {
                list.add(waypoint);

            }
            int value = 133;

            a += value;
            b += value;
            c += value;
            d += value;
            e += value;
            f += value;
            g += value;
            h += value;
            k += value;
            l += value;
            m += value;
            if (m > string.length()) {
                break;
            }
        }
        if(!list.isEmpty()) return list;
        else return null;
    }
    
    public static EnrouteWaypoint stringToObject(String string) {
        
        int a = 0, b = 1, c = 4, d = 6, e = 10, f = 12, g = 13, h = 18, k = 32, l = 41, m = 51;

            EnrouteWaypoint waypoint = new EnrouteWaypoint();
            waypoint.setArea(string.substring(b, c));
//            waypoint.setAeroport_code(string.substring(d, e));
            waypoint.setIcao_code(string.substring(19, 21));
            waypoint.setFix_id(string.substring(g, h));
            waypoint.setLatitude(string.substring(k, l));
            waypoint.setLongitude(string.substring(l, m));
        return waypoint;
    }


}
