/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polyline;
import rd.project.MainApp;
import rd.project.controller.Converter;
import rd.project.controller.SearchUtil;
import rd.project.model.Fix;
import rd.project.model.procedure.Star;
import rd.project.model.procedure.path_terminators.Leg;

/**
 *
 * @author ali
 */
public final class PlotView extends Parent {

    private final static double SCALE_COORD = 5000;
    private Map<String, Fix> mapFix;
    private double scale;
    private List<Point2D> fixList;
    private Polyline polyline = new Polyline();
    private Polyline polyline2;
    private Group group;
    private Pane pane;
    private Fix airportFix;
    private String airport_code;
    private String procedure_ident;
    private Star star;
    private double airportx;
    private double airporty;

    public PlotView() {
        this.fixList = new ArrayList<>();
        this.scale = 1.d;
    }

    public PlotView(String airport_code, String procedure_ident) {
        this.procedure_ident = procedure_ident;
        this.airport_code = airport_code;
        this.fixList = new ArrayList<>();
        this.scale = 1.d;
        init();
        group = draw();
        pane.getChildren().add(group);
        this.getChildren().add(pane);
    }

    public void init() {
        this.pane = new Pane();
        this.group = new Group();
        List<String> list = null;
        try {
            list = SearchUtil.getProceduresByICAOAndProcedureIdent(airport_code, procedure_ident, 'E');
        } catch (NullPointerException ex) {
            System.out.println("Null pointer exception cought");
        }
        if (!list.isEmpty()) {
            Converter converter = new Converter();
            this.star = new Star(list);
            mapFix = new HashMap<>();
            String fix_id = "";
            String fix_icao_code = "";
            Fix fix = null;

            for (Leg leg : star.getProcedureAsList()) {
                fix_id = leg.getFix_id();
                fix_icao_code = leg.getFix_icao_code();
                fix = SearchUtil.getFixObject(fix_id, fix_icao_code);
                mapFix.put(fix_id, fix);
            }
//            mapFix.entrySet().stream().forEach((a) -> {
//                System.out.println("fix id: " + a.getKey() + " |fix obj: " + a.getValue());
//            });

            airportFix = SearchUtil.getFixObject(star.getAeroport_code());
            airporty = -converter.hourMinSec_to_Decimal(airportFix.getLatitude()) * SCALE_COORD;
            airportx = converter.hourMinSec_to_Decimal(airportFix.getLongitude()) * SCALE_COORD;

        }
    }

    public Group draw() {
        if(star == null){return group;}
        this.scale = 1.d / scale;
        Converter converter = new Converter();
        polyline2 = new Polyline();
        double x = 0.0;
        double y = 0.0;
        double yy = 0.0;
        for (Leg leg : star.getProcedureAsList()) {
//            yy = Math.log(Math.tan((Math.PI/4.0) + 0.5 * y)); //Mercator projection
//            x = -converter.convertToCartesianX( mapFix.get(leg.getFix_id()))*SCALE_COORD;
//            y = -converter.convertToCartesianY( mapFix.get(leg.getFix_id()))*SCALE_COORD;
            
            x = converter.hourMinSec_to_Decimal(mapFix.get(leg.getFix_id()).getLongitude()) * SCALE_COORD;
            y = converter.hourMinSec_to_Decimal(mapFix.get(leg.getFix_id()).getLatitude());
            yy = -Math.log(Math.tan((Math.PI / 4.0) + 0.5 * y)) * SCALE_COORD;
//            System.out.println("fix id:" + mapFix.get(leg.getFix_id()));
//            System.out.println("x: " + x);
//            System.out.println("y: " + y);
//            System.out.println("yy: " + yy);
            polyline2.getPoints().addAll(new Double[]{x, yy});
            group.getChildren().add(circle(x, yy));
        }
        centreTheView();
        polyline2.setStrokeWidth(scale);
        Circle arp = circle(airportx, airporty);
        arp.setFill(Color.AZURE);
        this.group.getChildren().add(arp);
        this.group.getChildren().add(polyline2);
//        this.group.setScaleX(5);
//        this.group.setScaleY(5);
        
//        for(double point: polyline2.getPoints()){
//            
//        }
//        System.out.println(polyline2.getPoints().);
        return group;
    }

    public void update() {
        pane.getChildren().clear();
        group.getChildren().clear();
        this.getChildren().clear();
        group = draw();
        System.out.println("scale in PlotView: " + scale);
        pane.getChildren().add(group);
        this.getChildren().add(pane);
    }

    private void centreTheView() {
        System.out.println("airportx" + airportx);
        group.setTranslateX(100 - airportx);
        group.setTranslateY(100 - airporty);
    }

    private Circle circle(double x, double y) {
        Circle circle = new Circle(x, y, 3 * scale);
        return circle;

    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public String getAirport_code() {
        return airport_code;
    }

    public void setAirport_code(String airport_code) {
        this.airport_code = airport_code;
    }

}
