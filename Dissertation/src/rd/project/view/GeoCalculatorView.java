/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Border;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import rd.project.controller.GeoCalculator;
import rd.project.model.Fix;

/**
 *
 * @author Rustam Drake
 */
public class GeoCalculatorView extends Parent {

    GridPane gridPane;
    TextField resultTextField;

    public GeoCalculatorView() {
        this.gridPane = new GridPane();

        init();
    }

    private void init() {
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10));

        Label latLabel1 = new Label("Lat 1");
        gridPane.add(latLabel1, 0, 0);
        TextField latTextField1 = new TextField();
        latTextField1.setPromptText("use ARINC format");
        latTextField1.setMaxWidth(150);
        gridPane.add(latTextField1, 0, 1);

        Label lonLabel1 = new Label("Lon 1");
        lonLabel1.setMaxWidth(150);
        gridPane.add(lonLabel1, 1, 0);
        TextField lonTextField1 = new TextField();
        lonTextField1.setMaxWidth(150);
        gridPane.add(lonTextField1, 1, 1);

        Label latLabel2 = new Label("Lat 2");
        gridPane.add(latLabel2, 0, 2);
        TextField latTextField2 = new TextField();
        latTextField2.setMaxWidth(150);
        gridPane.add(latTextField2, 0, 3);

        Label lonLabel2 = new Label("Lat 2");
        gridPane.add(lonLabel2, 1, 2);
        TextField lonTextField2 = new TextField();
        gridPane.add(lonTextField2, 1, 3);

        Button resultButton = new Button("Result [ NM ]");
        gridPane.add(resultButton, 2, 3);
        resultButton.setOnAction((a) -> {
            
            Fix fix1 = new Fix();
            fix1.setLatitude(latTextField1.getText().toUpperCase());
            fix1.setLongitude(lonTextField1.getText().toUpperCase());
            Fix fix2 = new Fix();
            fix2.setLatitude(latTextField2.getText().toUpperCase());
            fix2.setLongitude(lonTextField2.getText().toUpperCase());
            
            try{
                resultTextField.setText(GeoCalculator.distanceBetweenTwoPoints(fix1, fix2) + "");
                resultTextField.setStyle("-fx-text-inner-color: black;");
            }catch(NumberFormatException ex){
                resultTextField.setText("Not The Right Format");
                resultTextField.setStyle("-fx-text-inner-color: red;");
            }
        });

        Label resultLabel = new Label("Result");
        resultLabel.setMaxWidth(100);
        gridPane.add(resultLabel, 2, 0);
        resultTextField = new TextField();
        resultTextField.setEditable(false);
        gridPane.add(resultTextField, 2, 1);
        resultTextField.setOnKeyPressed((key) -> {
            if (key.getCode() == KeyCode.ENTER) {

                double value1 = Double.parseDouble(latTextField1.getText());
                double value2 = Double.parseDouble(lonTextField1.getText());
                resultTextField.setText(value1 + value2 + "");
            }
        });

        this.getChildren().add(gridPane);
    }
}
