/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.view;

import java.util.List;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rd.project.controller.SearchUtil;
import rd.project.model.Fix;

/**
 *
 * @author Rustam Drake
 */
public class SearchView extends Parent {

    private final GridPane gridPane;
    private final TextField fix_id_TextField;
    private final TextField airport_id_TextField;
    private final TextField procedure_id_TextField;
    private final ScrollPane scrollPane;
    private final SplitPane splitPane;
//    private double widthOfWindow;

    public SearchView(Stage stage) {
//        widthOfWindow = stage.getWidth();
        splitPane = new SplitPane();
        splitPane.setOrientation(Orientation.VERTICAL);
        splitPane.setDividerPositions(0.1f, 0.6f);
        splitPane.disableProperty();
        splitPane.setStyle("-fx-box-border: transparent;");
        splitPane.setPrefSize(stage.getWidth() - 15, stage.getHeight() - 100);

        scrollPane = new ScrollPane();
        scrollPane.minWidth(500);
        scrollPane.minHeight(200);
        scrollPane.setPrefViewportWidth(500);
        scrollPane.setPrefViewportHeight(200);

        Button searchFix = new Button("Search");
        Label fix_id_label = new Label("Fix ID");
        fix_id_TextField = new TextField();

        Button searchProcedure = new Button("Search");
        Label ICAO_id_label = new Label("Airport");
        airport_id_TextField = new TextField();
        procedure_id_TextField = new TextField();
        procedure_id_TextField.setPromptText("Procedure Ident");

        gridPane = new GridPane();
        gridPane.maxHeightProperty().bind(splitPane.widthProperty().multiply(0.10));
        gridPane.setPadding(new Insets(5));
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        ColumnConstraints columnConstraints1 = new ColumnConstraints(50);
        ColumnConstraints columnConstraints2 = new ColumnConstraints(100);
        ColumnConstraints columnConstraints3 = new ColumnConstraints(50);
        ColumnConstraints columnConstraints4 = new ColumnConstraints(100);
        ColumnConstraints columnConstraints5 = new ColumnConstraints(100);
        columnConstraints2.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().addAll(columnConstraints1, columnConstraints2,
                columnConstraints3, columnConstraints4, columnConstraints5);

        GridPane.setHalignment(fix_id_label, HPos.RIGHT);
        gridPane.add(fix_id_label, 0, 0);

        GridPane.setHalignment(fix_id_TextField, HPos.LEFT);
        gridPane.add(fix_id_TextField, 1, 0);

        GridPane.setHalignment(searchFix, HPos.RIGHT);
        gridPane.add(searchFix, 1, 1);
        ////
        GridPane.setHalignment(ICAO_id_label, HPos.RIGHT);
        gridPane.add(ICAO_id_label, 2, 0);

        GridPane.setHalignment(airport_id_TextField, HPos.LEFT);
        gridPane.add(airport_id_TextField, 3, 0);

        GridPane.setHalignment(procedure_id_TextField, HPos.LEFT);
        gridPane.add(procedure_id_TextField, 4, 0);

        GridPane.setHalignment(searchProcedure, HPos.RIGHT);
        gridPane.add(searchProcedure, 3, 1);

        //event handellers below
        searchFix.setOnAction((ActionEvent event) -> {
            output_fix();
        });

        fix_id_TextField.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                output_fix();
            }
        });

        searchProcedure.setOnAction((event) -> {
            output_procedures();
        });

        airport_id_TextField.setOnKeyPressed((key) -> {
            if (key.getCode() == KeyCode.ENTER) {
                output_procedures();
            }
        });

        splitPane.getStylesheets().add(this.getClass().getResource("Text.css").toExternalForm());
        splitPane.getItems().add(gridPane);
        splitPane.getItems().add(scrollPane);
        getChildren().add(splitPane);
    }

    private void output_fix() {
        Text message = new Text();
        List<Fix> info = SearchUtil.getFixObjects(fix_id_TextField.getText().toUpperCase());
        if (info == null || info.isEmpty()) {
            message.setText("No Match Found");
        } else {
            String str = "";
            str = info.stream().map((fix) -> fix.toString() + "\n").reduce(str, String::concat);
            message.setText(str);
        }
        scrollPane.setContent(message);
    }

    private void output_procedures() {
        String temp = "";
        Text message = new Text();
//        message.setEditable(false);
        message.setStyle("-fx-background-color: transparent;"
                + " -fx-background-insets: 0; -fx-background-radius: 0;"
                + " -fx-padding: 0;");
        message.minWidth(600);
        message.prefHeight(BASELINE_OFFSET_SAME_AS_HEIGHT);
        message.setId("message1");
        System.out.println("");
        if (procedure_id_TextField.getText().equals("")) {
            message.setText(SearchUtil.getProceduresByICAO(airport_id_TextField.getText().toUpperCase()));
        } else {
            List<String> list = SearchUtil.getProceduresByICAOAndProcedureIdent(airport_id_TextField.getText().toUpperCase(), 
                    procedure_id_TextField.getText().toUpperCase(), 'E');
            temp = list.stream().map((s) -> s +"\n").reduce(temp, String::concat);
            message.setText(temp);

        }

//        System.out.println("ICAO value: " + airport_id_TextField.getText());
        scrollPane.setContent(message);
    }

}
