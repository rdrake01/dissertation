/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.controller;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rustam Drake
 */
public class ConverterTest {

    /**
     * Test of hourMinSec_to_Decimal method, of class Converter.
     */
    @Test
    public void testHourMinSec_to_Decimal() {
        String hourMinSec = "N06380970";
        Converter instance = new Converter();
        double expResult = 0.11582053397602482;
        double result = instance.hourMinSec_to_Decimal(hourMinSec);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testHourMinSec_to_Decimal_negativeValue() {
        String hourMinSec = "W031431201";
        Converter instance = new Converter();
        double expResult = -0.5536184872139694;
        double result = instance.hourMinSec_to_Decimal(hourMinSec);
        assertEquals(expResult, result, 0.0);
    }

}
