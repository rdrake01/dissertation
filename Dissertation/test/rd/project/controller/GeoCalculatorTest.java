/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.controller;

import org.junit.Test;
import static org.junit.Assert.*;
import rd.project.model.Fix;
import rd.project.model.TerminalWaypoint;

/**
 *
 * @author Rustam Drake
 */
public class GeoCalculatorTest {
    
    @Test
    public void testDistanceBetweenTwoPoints_Fix_Fix() {
        System.out.println("distanceBetweenTwoPoints");
        Fix fix1 = new TerminalWaypoint();
        fix1.setLatitude("N50035900");
        fix1.setLongitude("W005425300");
        Fix fix2 = new TerminalWaypoint();
        fix2.setLatitude("N58383800");
        fix2.setLongitude("W003041200");
        GeoCalculator instance = new GeoCalculator();
        double expResult = 523.13906;
        double result = instance.distanceBetweenTwoPoints(fix1, fix2);
        assertEquals(expResult, result, 0.0);
    }
    

    /**
     * Test of distanceBetweenTwoPoints method, of class GeoCalculator.
     */
    @Test
    public void testDistanceBetweenTwoPoints_4args() {
        System.out.println("distanceBetweenTwoPoints");
        String lat1 = "N51372331";
        String lon1 = "W000310688";
        String lat2 = "N51345905";
        String lon2 = "W000062425";
        GeoCalculator instance = new GeoCalculator();
        double expResult = 15.54546;
        double result = instance.distanceBetweenTwoPoints(lat1, lon1, lat2, lon2);
        assertEquals(expResult, result, 0.0);
    }
    
    
}
