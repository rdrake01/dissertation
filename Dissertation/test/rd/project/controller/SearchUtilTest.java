/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.controller;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rd.project.model.procedure.StarTest;
import rd.project.util.FileUtil;


public class SearchUtilTest {
    
    public SearchUtilTest() {
        try {
            FileUtil.readFile(new File("res/Star-VCBI-ANUT2A.ari"));
        } catch (IOException ex) {
            Logger.getLogger(StarTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFixObject method, of class SearchUtil.
     */
    @Test
    public void testGetFixInfo() {
        String expResult = "ANUTI";
        String result = SearchUtil.getFixObject("ANUTI").getFix_id();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetProceduresByICAOAndProcedureIdent() {
        String expResult = "SMESP VCBIVCEANUT2A5RW04  001ANUTIVCPC1E    010IF                                             13000                        000611609";
        String result = SearchUtil.getProceduresByICAOAndProcedureIdent("VCBI", "ANUT2A", 'E').get(0);
        System.out.println("result: "+result);
        assertEquals(expResult, result);
    }
    
}
