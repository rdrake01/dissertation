///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package rd.project.util;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Rustam Drake
// */
//public class Arinc424UtilTest {
//
//    List<String> expResult = new ArrayList<>();
//    File file = new File("res/test_file_reader_two_lines.ari");
//
//    public Arinc424UtilTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of getAllWaipointNames method, of class Arinc424Util.
//     */
//    @Test
//    public void testReturnOnlyWaypointsFromFile() throws IOException{
//        File file = new File("res/Star-VCBI-ANUT2A.ari");
//        Arinc424Util instance = new Arinc424Util(FileUtil.readFile(file));
////        List<String> list = instance.ftomStringToObject();
//        System.out.println("testing big file: "+instance.ftomStringToObject());
//    }
//    
//    @Test
//    public void testGetAllWaipointNames() throws IOException {
//        Arinc424Util instance = new Arinc424Util(FileUtil.readFile(file));
//        System.out.println("waypoint object: "+instance.ftomStringToObject().toString());
//        
//        expResult.add("136N ");
//        expResult.add("154N ");
//        List<String> result = instance.getAllWaipointNames();
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of getAriFileAsString method, of class Arinc424Util.
//     */
//    @Test
//    public void testGetAriFileAsString() throws IOException {
//        System.out.println("getAriFileAsString");
//        Arinc424Util instance = new Arinc424Util(FileUtil.readFile(file));
//        String expResult = ""
//                + "SEEUP UAUUUAC136N  UA1    I Z   N53225052E063213935         "
//                + "              E0122                T  136N                  "
//                + "   000371510"
//                + "SEEUP UAUUUAC154N  UA1    I Z   N53243157E063281284         "
//                + "              E0122                T  154N                  "
//                + "   000391510";
//        String result = instance.getAriFileAsString();
//        assertEquals(expResult, result);
//    }
//
//}
