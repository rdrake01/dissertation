/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.util;

import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rustam Drake
 */
public class FileUtilTest {

    public FileUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of readFile method, of class FileUtil.
     */
    @Test
    public void testReadFile() throws Exception {
        System.out.println("readFile");
        File file = new File("res/test_file_reader.ari");
        String expResult = "SEEUP UAUUUAC136N  UA1    "
                + "I Z   N53225052E063213935                       "
                + "E0122                "
                + "T  136N                     000371510";
        String result = FileUtil.readFile(file);
        assertEquals(expResult, result);
    }

    @Test
    public void testReadFile_multipleLines() throws Exception {
        File file = new File("res/test_file_reader_two_lines.ari");
        String expResult = "SEEUP UAUUUAC136N  UA1    I Z   N53225052E063213935"
                + "                       E0122                T  136N         "
                + "            000371510"
                + "SEEUP UAUUUAC154N  UA1    I Z   N53243157E063281284           "
                + "            E0122                T  154N                    "
                + " 000391510";
        String result = FileUtil.readFile(file);
        assertEquals(expResult, result);
        System.out.println(result);

    }

    @Test(expected = IOException.class)
    public void testReadFile_IOExeption() throws IOException {
        System.out.println("readFile IOExeption");
        File file = new File("res/FILE_DOESN'T_EXIST.ari");
        String expResult = "SEEUP UAUUUAC136N  UA1    "
                + "I Z   N53225052E063213935                       "
                + "E0122                "
                + "T  136N                     000371510";
        String result = FileUtil.readFile(file);
        assertEquals(expResult, result);
    }
    @Test
    public void test1() throws IOException {
        File file2 = new File("res/Star-VCBI-ANUT2A.ari");
        String fu = FileUtil.readFile(file2);
    }

}
