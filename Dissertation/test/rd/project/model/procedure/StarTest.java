/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rd.project.model.procedure;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;
import rd.project.controller.SearchUtil;
import rd.project.util.FileUtil;

/**
 *
 * @author rustam.drake
 */
public class StarTest {
    public StarTest() {
        try {
            FileUtil.readFile(new File("res/Star-VCBI-ANUT2A.ari"));
        } catch (IOException ex) {
            Logger.getLogger(StarTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    /**
     * Test of getProcedureAsList method, of class Star.
     */
    @Test
    public void testGetProcedureAsList() {
        System.out.println("getProcedureAsList");
        List<String> procedureList 
                = SearchUtil.getProceduresByICAOAndProcedureIdent("VCBI", "ANUT2A", 'E');
        Star instance = new Star(procedureList);
        String expResult = "BI733";
        String result = instance.getProcedureAsList().get(1).getFix_id();
        assertEquals(expResult, result);
    }
    
}
